Imports CommonShiny
Imports BizShiny
Imports System.Data

Partial Class Gallery
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        LoadDataList()

    End Sub

    Private Sub LoadDataList()

        Dim bg As New BizGroup()
        Dim ds As DataSet
        ds = bg.findGroup("", "", "")
        dt_Group.DataSource = ds
        dt_Group.DataBind()

    End Sub

    Function GetImg(ByVal IID)
        Dim bi As New BizImage()
        Dim ds As DataSet

        ds = bi.findImage("", "", "", IID)

        If ds Is Nothing Then
            Return Nothing
        Else
            Return ds
        End If

    End Function

    Function RelDte(ByVal dte)

        Dim InsDate As String
        InsDate = Right(dte, 2) + "/" + Mid(dte, 5, 2) + "/" + Left(dte, 4)

        Return InsDate

    End Function

End Class
