﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="index.aspx.vb" Inherits="index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/shine_home.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>SHINE Guidance Centre | one-stop multidisciplinary centre for children and adolescents with special needs</title>
<!-- InstanceEndEditable -->
<script>
</script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>

<script type="text/javascript">
$(document).ready(function() {	

		var id = '#dialog';
	
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
	
		//transition effect
		$(id).fadeIn(2000); 	
	
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});		
	
});

</script>

<!-- InstanceBeginEditable name="head" -->
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
<meta NAME="KEYWORDS" CONTENT="Special needs, One-stop centre,Early intervention, Special education, Speech therapy,Occupational therapy, Music therapy, Clinical child psychology, Assessments, Autism, Asperger’s, Learning difficulties, Learning disabilities, Developmental delays, ADD/ADHD, Speech, language and communication, Speech therapist, Occupational therapist, Clinical psychologist, Special education teacher, Behaviour management and modification, Parent training, School readiness programme, Self-help skills, Play skills, Social skills, Speech delay, Diagnosis, Malaysia" />
<!-- InstanceEndEditable -->
</head>

<body onload="openWindow();MM_preloadImages('images/btn_01_ro.jpg','images/btn_02_ro.jpg','images/btn_03_ro.jpg','images/btn_04_ro.jpg','images/btn_05_ro.jpg','images/btn_06_ro.jpg','images/btn_07_ro.jpg','images/btn_08_ro.jpg')">
<div class="wrapper">
<table class="page_frame" border="0" cellpadding="0" cellspacing="0" align="center" >
  <tr>
   <td background="images/header_01.jpg"></td>
  </tr>
  <tr>
     <td background="images/header_02.jpg"><table width="960" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td height="84" style="width: 42px">&nbsp;</td>
         <td width="186" height="84"><a href="index.aspx"><img src="images/logo.jpg" alt="Logo" width="186" height="84" border="0" /></a></td>
         <td background="images/slogan.jpg" width="372" height="84">&nbsp;</td>
         <td background="images/address.jpg" width="185" height="84">&nbsp;</td>
         <td background="images/phone.jpg" width="133" height="84">&nbsp;</td>
         <td width="39" height="84">&nbsp;</td>
       </tr>
     </table></td>
  </tr>
  <tr>
    <td background="images/divide.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td width="41" height="19" class="../html/header_bgimage03"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="nav_bar">
            <div class="mainmenu" align="center">
            <ul>
                <li><a href="#" id="maincurrent">HOME</a></li>
                <li><a href="html/about.html">ABOUT US</a></li>
                <li><a href="html/services.html">OUR SERVICES</a></li>
                <li><a href="html/team.html">OUR TEAM</a></li>
                <li><a href="html/contact.html">CONTACT</a></li>
                <li><a href="html/faq_enrolment.html">FAQ</a></li>
                <li><a href="html/career.html">CAREER</a></li>
                <li><a href="gallery.aspx">GALLERY</a></li>
                <li style="padding-right:0px; margin-right:0px;"><a href="html/upcoming_event.html">EVENTS / PROGRAMS</a></li>
            </ul>
    	</div>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
   <td background="images/header_04.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="42" height="264">&nbsp;</td>
        <td valign="top"><!-- InstanceBeginEditable name="Edit03" -->
        <img src="images/home_banner.png" /> 
        <!-- InstanceEndEditable --></td>
        <td width="39" height="64">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
    <table width="90%" border="0" cellspacing="0" cellpadding="0" style="height:150px;">
      <tr>
        <td>&nbsp;</td>
        <td width="100%" align="left">
            <strong><!--<span style="color: #3f403f; font-family: Arial">Our Mission</span>--></strong></td>
        <td>&nbsp;&nbsp;&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td valign="top" align="left">
        <div class="content" style="text-align:left; line-height:24px;"><strong style="color:#000;">SHINE Guidance Centre</strong> is a one-stop multidisciplinary centre for children
            and adolescents with special needs. The centre is wholly owned by <strong style="color:#000;">Nilai Healthcare Services Sdn. Bhd.</strong>
            and was initiated by <strong style="color:#000;">Tan Sri Dato Dr Gan Kong Seng</strong>, the <strong  style="color:#000;">Executive
            Chairman of Nilai Resources Group Berhad</strong>.
            <br /><br /><div align="right" style="color:#3f403f; font-size:8pt"><a href="html/about.html">Read More</a></div>
        </div>
            </td>
            <td>&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr class="footer_position">
    <td width="42" height="57" class="header_bgimage05"><table border="0" cellspacing="0" cellpadding="0" style="width: 960px;">
      <tr>
        <td class="footer_copyright" style="height: 19px; width: 104px;">&nbsp;</td>
        <td class="footer_copyright" style="width: 2294px; height: 19px">@ 2012 SHINE Guidance Centre </td>
        <td style="width: 1603px; height: 19px">&nbsp;</td>
          <td style="height: 19px; width: 442px;">
          </td>
          <td style="height: 19px; width: 141px;">
          </td>
      </tr>
      <tr>
        <td class="footer_add" style="height: 12px; width: 104px;">&nbsp;</td>
        <td class="footer_add" style="width: 2294px; height: 12px" valign="top">Jaya One, J-29-2, Block J No. 72A, Jalan Universiti, Petaling Jaya, 46200, Selangor.</td>
        <td class="footer_add" style="width: 1603px; height: 12px" valign="top">Office : 03 - 7960 8809 / 7960 9809   
            
            FAX : 03 - 7960 6809 </td>
          <td class="footer_add" style="height: 12px; width: 442px;" valign="top">
              Email: info@shine.my</td>
          <td class="footer_add" style="height: 12px; width: 141px;">
          <a href="http://www.quality-direct.net" style="text-decoration:none;"><img src="images/web_design.png" alt="web design by Quality Direct" border="0"/></a>
          </td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
<!-- InstanceEnd --></html>

