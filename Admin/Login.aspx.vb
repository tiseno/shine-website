Imports CommonShiny
Imports BizShiny
Imports DataShiny
Imports System.Data

Partial Class Admin_Login
    Inherits System.Web.UI.Page

    Protected Sub butLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butLogin.Click
        Dim bm As New BizAdmin()
        Dim status As DataSet
        Try
            status = bm.logIn(Me.texLoginID.Text, Me.texPassword.Text)
            Dim iid As String = ""
            If (Not (status Is Nothing) And Not (status.Tables.Count = 0) And Not (status.Tables(0).Rows.Count = 0)) Then
                iid = status.Tables(0).Rows(0)(0).ToString()
            End If

            If Not iid = "" Then
                Session("LoginID") = Me.texLoginID.Text
                Response.Redirect("Admin.aspx")
            Else
                labMessage.Text = "Login failed. Please try again or consult with your IT Administrator."
            End If

        Catch ex As Exception
            Me.errorMessage.Style("display") = "block"
            Me.labError.Text = "ERROR : " & ex.Message.ToString()
        End Try
    End Sub
End Class
