Imports System.IO
Imports BizShiny

Partial Class Group_new
    Inherits System.Web.UI.Page

    Protected Sub btn_create_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_create.Click
        Dim Gname As String = txt_gname.Text
        Dim Gdesc As String = txt_gdesc.Value
        Dim bg As New BizGroup()
        Dim script As String = Nothing
        Dim message As String = Nothing

        If Not Gname = "" Then
            Try
                Dim gid As Integer
                gid = bg.insertGroup(Gname, Gdesc)

                message = "Created successfully."
                script = "<script type=""text/javascript"">alert('" & message & "');window.location='Group_photo.aspx?iid=" & gid & "';</script>"
                Me.ClientScript.RegisterStartupScript(Me.GetType(), "Group", script)

            Catch ex As Exception

            End Try
        Else
            Me.lblMsg.Text = "Please specify Group Name."
        End If

    End Sub
End Class
