<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Admin_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SHINE Child Guidance Centre | Admin</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #d1d1d1;
	margin-top: 30px;
	margin-left: 30px;
}
-->
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        &nbsp;</div>
    <table width="100%">
        <tr align="center">
            <td>
        <img src="../images/logo.jpg" /><br />
            </td>
        </tr>
        <tr align="center">
            <td style="height: 23px">
            </td>
        </tr>
    <tr align="center">
        <td>
            <table style="background: whitesmoke; border-top-style: solid; border-right-style: solid;
            border-left-style: solid; border-bottom-style: solid">
            <tr>
                <td align="right" style="height: 26px">
                    <asp:Label ID="Label1" runat="server" ForeColor="Black" Text="Login ID : " Font-Names="verdana" Font-Size="8pt"></asp:Label>
                </td>
                <td align="left" style="width: 158px; height: 26px">
                    <asp:TextBox ID="texLoginID" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label2" runat="server" ForeColor="Black" Text="Password : " Font-Names="verdana" Font-Size="8pt"></asp:Label>
                </td>
                <td align="left" style="width: 158px">
                    <asp:TextBox ID="texPassword" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="left" style="width: 158px">
                    <asp:Button ID="butLogin" runat="server" Text="Log In" />
                </td>
            </tr>
            <tr>
                <td align="right" style="height: 40px">
                </td>
                <td style="width: 158px; height: 40px">
                    <div id="errorMessage" runat="server" style="display: none; color: white">
                        &nbsp;</div>
                    <asp:Label ID="labError" runat="server" ForeColor="Black" Font-Names="verdana" Font-Size="8pt"></asp:Label>
                    <asp:Label ID="labMessage" runat="server" ForeColor="Black" Font-Names="verdana" Font-Size="8pt"></asp:Label>
                </td>
            </tr>
        </table>
        </td>
    </tr>
        <tr align="center">
            <td style="height: 48px">
                <span class="content">.: Web Control Panel :. Copyright 2009 SHINE Child Guidance Centre. All rights reserved.</span>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
