<%@ Page Language="VB" AutoEventWireup="false" CodeFile="News_display.aspx.vb" Inherits="News_display" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript" src="../firebug/firebug.js"></script>
    <script type="text/javascript" src="../scripts/jquery.min.js"></script>
    <script type="text/javascript" src="../scripts/date.js"></script>
    <script type="text/javascript" src="../scripts/jquery.datePicker.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="../styles/datePicker.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../styles/demo.css" />
    <script type="text/javascript" charset="utf-8">
            $(function()
            {
				$('.date-pick').datePicker({startDate:'01/01/1996'});

            });
		</script>
		
	<script src="/mint/?js" type="text/javascript"></script>
	<style type="text/css">
        body,input,form,span,div,select{font-family:Tahoma;font-size:9pt;}
    </style>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body bgcolor="whitesmoke">
    <form id="form1" runat="server">
    <div>
        &nbsp;<a class="subline01">News</a><br />
        <br />
        <table>
            <tr>
                <td style="width: 91px; height: 23px;">
                    Title</td>
                <td style="width: 12px; height: 23px;">
                    :</td>
                <td style="width: 182px; height: 23px;">
                    <asp:TextBox ID="txt_title" runat="server"></asp:TextBox></td>
                <td style="width: 6px; height: 23px;">
                </td>
                <td style="width: 193px; height: 23px;">
                </td>
            </tr>
            <tr>
                <td style="width: 91px;">
                    Insertion Date</td>
                <td style="width: 12px;">
                    :</td>
                <td style="width: 182px;">
                <input name="date1" id="date1" runat="server" readonly="readonly" class="date-pick" />
                </td>
                <td style="width: 6px;">
                    -</td>
                <td style="width: 193px;">
                <input name="date2" id="date2" runat="server" readonly="readonly" class="date-pick" /></td>
            </tr>
            <tr>
                <td style="width: 91px">
                </td>
                <td style="width: 12px">
                </td>
                <td style="width: 182px">
                    <asp:Button ID="btn_srch" runat="server" Text="Search News" Width="103px" /></td>
                <td style="width: 6px">
                </td>
                <td style="width: 193px">
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="gvNews" runat="server" AutoGenerateColumns="False" OnRowDeleting="gvNewsDeleteRow" DataKeyNames="NewsID" BackColor="White" Width="100%">
            <HeaderStyle  BackColor="Gray" ForeColor="White" />
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="NewsID" DataNavigateUrlFormatString="News_edit.aspx?iid={0}" Text="Edit" />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton id="btnDelete" CommandName="Delete" Runat="server" Text="Delete" OnClientClick="javascript : return confirm('Do you really want to \ndelete the item?');"/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="NewsTitle" HeaderText="Topic" >
                    <ItemStyle Width="450px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Insertion">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# RelDte(DataBinder.Eval(Container, "DataItem.NewsInsertionDate")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
