Imports BizShiny
Imports System.Data
Imports System.IO

Partial Class News_display
    Inherits System.Web.UI.Page

    Protected Sub btn_srch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_srch.Click
        Dim bn As New BizNews()
        Dim dt As DataSet
        Try
            dt = bn.findNews(txt_title.Text, date1.Value, date2.Value)
            gvNews.DataSource = dt
            gvNews.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Function RelDte(ByVal dte)

        Dim InsDate As String
        InsDate = Right(dte, 2) + "/" + Mid(dte, 5, 2) + "/" + Left(dte, 4)

        Return InsDate

    End Function

    Protected Sub gvNewsDeleteRow(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)

        Dim nid As String = gvNews.DataKeys(e.RowIndex).Values(0).ToString()
        'Response.Write(nid)
        Dim bn As New BizNews()
        Dim img As DataSet
        img = bn.selectImages(nid)
        Dim cnt As Integer = img.Tables(0).Rows.Count
        Dim d As Integer
        Dim i_path As String = ""
        Dim iid As String = ""

        'Dim msg As String
        'Dim title As String
        'Dim style As MsgBoxStyle
        'Dim response As MsgBoxResult
        'msg = "Confirm delete news and its content?"   ' Define message.
        'style = MsgBoxStyle.DefaultButton2 Or _
        '   MsgBoxStyle.Critical Or MsgBoxStyle.YesNo
        'title = "Delete News"   ' Define title.
        '' Display message.
        'response = MsgBox(msg, style, title)
        'If response = MsgBoxResult.Yes Then   ' User chose Yes.

        For d = 0 To cnt - 1
            i_path = Server.MapPath("../" + img.Tables(0).Rows(d)(1).ToString())
            File.Delete(i_path)
            iid = img.Tables(0).Rows(d)(0).ToString()
            bn.deleteImage(iid)
        Next

        bn.deleteNews(nid)

        'Else
        '' Perform some other action.
        'End If

    End Sub

End Class
