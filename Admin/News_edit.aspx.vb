Imports System.IO
Imports CommonShiny
Imports BizShiny
Imports System.Data

Partial Class News_edit
    Inherits System.Web.UI.Page

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        If ImageUpload.HasFile Then

            Dim fn As String = Nothing
            Dim c As String = Nothing
            Dim Nid As Integer = Request("iid")

            Try
                Dim strFileName As String
                strFileName = ImageUpload.PostedFile.FileName
                c = System.IO.Path.GetFileName(strFileName)
                c = c.Substring(0, c.LastIndexOf("."))
                Dim d As String = System.IO.Path.GetExtension(strFileName)
                Dim f As String = System.DateTime.Now.ToString("ddmmyy")
                Dim g As String = Left((System.DateTime.Now.ToString("hhmmssff")), 8)

                fn = f + g + c + d
                Dim SaveLocation As String = Server.MapPath("..\Img") & "\" & fn

                ImageUpload.PostedFile.SaveAs(SaveLocation)

            Catch ex As Exception
                Me.lblMsg.Text = "File upload FAILED."
            End Try

            Dim iPath As String = "Img" & "/" & fn
            Dim bn As New BizNews()
            bn.insertImage(c, "", iPath, Nid)

            LoadDataList(Nid)

        Else
            Me.lblMsg.Text = "Please specify a valid file."
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Dim Nid As Integer = Request("iid")
            Dim bg As New BizNews()
            Dim cn As CommonNews = bg.selectNewsByID(Nid)
            If Not cn Is Nothing Then
                txt_NewsTitle.Text = cn.Title
                FreeTextBox1.Text = cn.Body
            End If

            LoadDataList(Nid)

        End If
    End Sub

    Private Sub LoadDataList(ByVal IID As String)

        Dim bn As New BizNews()
        Dim dt As DataSet
        Try
            dt = bn.selectImages(IID)
            dlImages.DataSource = dt
            dlImages.DataBind()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btn_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Submit.Click

        Dim bn As New BizNews()
        Dim script As String = Nothing
        Dim message As String = Nothing
        Dim Nid As Integer = Request("iid")

        Try

            Dim niid As Integer
            niid = bn.updateNews(Nid, txt_NewsTitle.Text, FreeTextBox1.Text)

            message = "Updated successfully."
            script = "<script type=""text/javascript"">alert('" & message & "');window.location='News_display.aspx';</script>"
            Me.ClientScript.RegisterStartupScript(Me.GetType(), "News", script)

        Catch ex As Exception

        End Try


    End Sub
End Class
