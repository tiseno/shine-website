<%@ Page Language="VB" AutoEventWireup="false" CodeFile="News_new.aspx.vb" Inherits="News_new" ValidateRequest="false" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
<style type="text/css">
    body,input,form,span,div,select{font-family:Tahoma;font-size:9pt;}
</style>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div><a class="subline02">
        Create New News</a><br />
        <br />
        News Title : &nbsp; &nbsp; &nbsp; &nbsp;<asp:TextBox ID="txt_NewsTitle" runat="server"></asp:TextBox><br />
        <br />
            &nbsp;Images Gallery :
        <br />
        <br />
        <asp:Panel ID="Panel1" runat="server" Height="68px" ScrollBars="Vertical" >
            <asp:DataList ID="dlImages" runat="server" RepeatColumns="10" RepeatDirection="Horizontal" CellPadding="2" CellSpacing="0">
             <ItemTemplate>
                    <img src='../Img/<%# Eval("Name") %>' style="border:1px solid gray;"  height="60px" width="60px"/>
             </ItemTemplate>
            </asp:DataList>
        </asp:Panel>
        <br />
        Upload Image to Gallery : &nbsp; &nbsp; &nbsp;
        <asp:FileUpload ID="ImageUpload" runat="server" />
        <asp:Button ID="btnUpload" runat="server" Text="Upload" />
        &nbsp; &nbsp;
        <asp:Label ID="lblMsg" runat="server"></asp:Label><br />
        <br />
    <FTB:FreeTextBox id="FreeTextBox1" width="100%" height="400px" runat="server" ToolbarStyleConfiguration="Office2000" BreakMode="LineBreak" EnableHtmlMode="true" />
        <br />
        &nbsp;<asp:Button ID="btn_Submit" runat="server" Text="Submit" /></div>
    </form>
</body>
</html>
