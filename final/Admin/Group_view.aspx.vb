Imports CommonShiny
Imports BizShiny
Imports DataShiny
Imports System.Data
Imports System.IO

Partial Class Admin_Group_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        BindData()

    End Sub

    Function RelDte(ByVal dte)

        Dim InsDate As String
        InsDate = Right(dte, 2) + "/" + Mid(dte, 5, 2) + "/" + Left(dte, 4)

        Return InsDate

    End Function

    Protected Sub gvGroupDeleteRow(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)

        'Dim loc As String = Server.MapPath("../gall/test.jpg")
        'File.Delete(loc)

        Dim gid As String = gvGroup.DataKeys(e.RowIndex).Values(0).ToString()
        Dim bi As New BizImage()
        Dim bg As New BizGroup()
        Dim img As DataSet
        img = bi.findImage("", "", "", gid)
        Dim cnt As Integer = img.Tables(0).Rows.Count
        Dim d As Integer
        Dim i_path As String = ""
        Dim iid As String = ""

        'Dim msg As String
        'Dim title As String
        'Dim style As MsgBoxStyle
        'Dim response As MsgBoxResult
        'msg = "Confirm delete group and its images?"   ' Define message.
        'style = MsgBoxStyle.DefaultButton2 Or _
        '   MsgBoxStyle.Critical Or MsgBoxStyle.YesNo
        'title = "Delete Group"   ' Define title.
        '' Display message.
        'response = MsgBox(msg, style, title)
        'If response = MsgBoxResult.Yes Then   ' User chose Yes.

        For d = 0 To cnt - 1
            i_path = Server.MapPath("../" + img.Tables(0).Rows(d)(2).ToString())
            File.Delete(i_path)
            iid = img.Tables(0).Rows(d)(0).ToString()
            bi.deleteImage(iid)
        Next d

        bg.deleteGroup(gid)

        BindData()

        'Else
        '' Perform some other action.
        'End If

    End Sub

    Private Sub BindData()

        Dim bg As New BizGroup()
        Dim ds As DataSet

        ds = bg.findGroup("", "", "")
        gvGroup.DataSource = ds
        gvGroup.DataBind()

    End Sub

End Class