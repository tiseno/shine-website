Imports CommonShiny
Imports BizShiny
Imports DataShiny
Imports System.Data
Imports System.IO
Imports System
Imports System.Windows


Partial Class Group_photo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then

            Dim Gid As Integer = Request("iid")
            Dim bg As New BizGroup()
            Dim cg As CommonGroup = bg.selectGroupByID(Gid)

            If Not cg Is Nothing Then
                txt_gname.Text = cg.GroupName
                txt_gdesc.Value = cg.GroupDescription
            End If

            BindData(Gid)

        End If
    End Sub
    Private Sub BindData(ByVal IID As String)

        Dim bi As New BizImage()
        Dim dt As DataSet
        Try
            dt = bi.findImage("", "", "", IID)
            gv_photo.DataSource = dt
            gv_photo.DataBind()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btn_ImgUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ImgUpload.Click


            If photoUpload.HasFile Then

                Dim fn As String = Nothing
                Dim groupid As String = Request("iid")

            Try

                Dim strFileName As String
                strFileName = photoUpload.PostedFile.FileName
                Dim c As String = System.IO.Path.GetFileName(strFileName)
                c = c.Substring(0, c.LastIndexOf("."))
                Dim d As String = System.IO.Path.GetExtension(strFileName)
                Dim f As String = System.DateTime.Now.ToString("ddmmyy")
                Dim g As String = Left((System.DateTime.Now.ToString("hhmmssff")), 8)
                Dim fSize As Integer = photoUpload.PostedFile.ContentLength / 1024

                If Not (d = ".jpg" Or d = ".jpeg" Or d = ".png" Or d = ".gif") Then

                    Me.lblMsg.Text = "Invalid Image Format!!!"

                ElseIf fSize > 500 Then

                    Me.lblMsg.Text = "Image Size Too Large!!! Upload Image Smaller than 500Kb."

                Else

                    fn = f + g + c + d
                    Dim SaveLocation As String = Server.MapPath("..\Gallery") & "\" & fn

                    photoUpload.PostedFile.SaveAs(SaveLocation)

                    Dim iName As String = txt_ImgName.Text
                    Dim iDesc As String = txt_ImgDesc.Value
                    Dim iPath As String = "Gallery" & "/" & fn

                    Dim bi As New BizImage()
                    bi.insertImage(iName, iDesc, iPath, groupid)

                    BindData(groupid)

                End If

            Catch ex As Exception
                Me.lblMsg.Text = "File upload FAILED."
            End Try


            Else
                Me.lblMsg.Text = "Please specify a valid file."
            End If

    End Sub

    Function RelDte(ByVal dte)

        Dim InsDate As String
        InsDate = Right(dte, 2) + "/" + Mid(dte, 5, 2) + "/" + Left(dte, 4)

        Return InsDate

    End Function

    Protected Sub gv_photoDeleteRow(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)

        'Dim msg As String
        'Dim title As String
        Dim groupid As String = Request("iid")
        'Dim style As MsgBoxStyle
        'Dim response As MsgBoxResult
        'msg = "Confirm delete image?"   ' Define message.
        'style = MsgBoxStyle.DefaultButton2 Or _
        '   MsgBoxStyle.Critical Or MsgBoxStyle.YesNo
        'title = "Delete Image"   ' Define title.
        '' Display message.
        'response = MsgBox(msg, style, title)
        'If response = MsgBoxResult.Yes Then   ' User chose Yes.
        Dim iid As String = gv_photo.DataKeys(e.RowIndex).Values(0).ToString()
        'response.Write(iid)
        Dim bi As New BizImage()
        Dim ci As CommonImage = bi.selectImageByID(iid)
        Dim i_path As String = Server.MapPath("../" + ci.ImagePath)
        File.Delete(i_path)
        bi.deleteImage(iid)
        'Response.Write(i_path)

        BindData(groupid)
        'Else
        '' Perform some other action.
        'End If

    End Sub

    Protected Sub btn_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_edit.Click

        Dim script As String = Nothing
        Dim message As String = Nothing
        Dim gid As String = Request("iid")
        Dim bg As New BizGroup()
        Dim gname As String = txt_gname.Text
        Dim gdesc As String = txt_gdesc.Value

        Try

            bg.updateGroup(gid, gname, gdesc)

            message = "Updated successfully."
            script = "<script type=""text/javascript"">alert('" & message & "');window.location='Group_view.aspx';</script>"
            Me.ClientScript.RegisterStartupScript(Me.GetType(), "Group", script)

        Catch ex As Exception

        End Try


    End Sub
End Class
