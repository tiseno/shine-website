<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Group_photo.aspx.vb" Inherits="Group_photo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
<style type="text/css">
    body,input,form,span,div,select{font-family:Tahoma;font-size:9pt;}
</style>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body bgcolor="whitesmoke">
    <form id="form1" runat="server">
    <div><a class="subline01">
        Gallery</a><br />
        <br />
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 86px; height: 2px">
                    Group Name</td>
                <td style="width: 2px; height: 2px">
                    :&nbsp;</td>
                <td style="width: 187px; height: 2px">
                    <asp:TextBox ID="txt_gname" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 86px; height: 35px" valign="top">
                    Description</td>
                <td style="width: 2px; height: 35px" valign="top">
                    :&nbsp;</td>
                <td style="width: 187px; height: 35px">
                    <textarea id="txt_gdesc" runat="server" rows="4" style="width: 220px"></textarea></td>
            </tr>
            <tr>
                <td style="width: 86px; height: 31px">
                </td>
                <td style="width: 2px; height: 31px">
                </td>
                <td style="width: 187px; height: 31px">
                    <asp:Button ID="btn_edit" runat="server" Text="Update" /></td>
            </tr>
        </table>
        <br /><a class="subline02">
        Add Images</a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        <br />
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 86px; height: 12px;">
                    </td>
                <td style="width: 6px; height: 12px;">
                    &nbsp;</td>
                <td style="width: 245px; height: 12px">
                    <asp:TextBox ID="txt_ImgName" runat="server" Visible="false" Height="4px"></asp:TextBox>
                    <textarea id="txt_ImgDesc" runat="server" visible="false" style="width: 57px; height: 1px"></textarea></td>
                <td style="height: 12px">
                </td>
            </tr>
            <tr>
                <td style="width: 86px; height: 8px">
                    Upload
                    Image</td>
                <td style="width: 6px; height: 8px">
                    :&nbsp;</td>
                <td style="width: 245px; height: 8px">
                    <asp:FileUpload ID="photoUpload" runat="server" Width="356px" /></td>
                <td style="height: 8px">
                </td>
            </tr>
            <tr>
                <td style="width: 86px; height: 31px;">
                </td>
                <td style="width: 6px; height: 31px;">
                </td>
                <td style="width: 245px; height: 31px;">
                    <asp:Button ID="btn_ImgUpload" runat="server" Text="Upload" />
        <asp:Label ID="lblMsg" runat="server" ForeColor="#FF0066"></asp:Label></td>
                <td style="height: 31px">
                </td>
            </tr>
        </table>
    
    </div>
    <br />
        <asp:GridView ID="gv_photo" runat="server" AutoGenerateColumns="False" DataKeyNames="ImageID" OnRowDeleting="gv_photoDeleteRow" BackColor="White">
            <HeaderStyle BackColor="Gray" ForeColor="White" />
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton id="btnDelete" CommandName="Delete" Runat="server" Text="Delete" OnClientClick="javascript : return confirm('Do you really want to \ndelete the item?');"/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Images">
                    <ItemTemplate>
                        <img src='../<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>' height="150px" width="150px"/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Image Name" DataField="ImageName" Visible="False" />
                <asp:BoundField HeaderText="Image Description" DataField="ImageDescription" Visible="False" />
                <asp:TemplateField HeaderText="Insertion Date">
                    <ItemTemplate>
                        <asp:Label ID="InsDate" runat="server" Text='<%# RelDte(DataBinder.Eval(Container, "DataItem.ImageInsertionDate")) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                </asp:TemplateField>
                
            </Columns>
        </asp:GridView>
    </form>
</body>
</html>
