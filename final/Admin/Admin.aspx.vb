
Partial Class Admin_Admin
    Inherits System.Web.UI.Page

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImageButton1.Click
        Dim frame1 As HtmlControl = CType(Me.FindControl("frame1"), HtmlControl)
        frame1.Attributes("src") = "Group_view.aspx"
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim frame1 As HtmlControl = CType(Me.FindControl("frame1"), HtmlControl)
        frame1.Attributes("src") = "Group_new.aspx"
    End Sub

    Protected Sub btnProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProduct.Click
        Dim frame1 As HtmlControl = CType(Me.FindControl("frame1"), HtmlControl)
        frame1.Attributes("src") = "News_display.aspx"
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim frame1 As HtmlControl = CType(Me.FindControl("frame1"), HtmlControl)
        frame1.Attributes("src") = "News_create.aspx"
    End Sub

    Protected Sub linSignOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linSignOut.Click
        Response.Redirect("login.aspx")
    End Sub
End Class
