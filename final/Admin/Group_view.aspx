<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Group_view.aspx.vb" Inherits="Admin_Group_view" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
<style type="text/css">
    body,input,form,span,div,select{font-family:Tahoma;font-size:9pt;}
</style>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body bgcolor="whitesmoke">
    <form id="form1" runat="server">
    <div><a class="subline01">
        Gallery Group</a><br />
        <br />
        <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" OnRowDeleting="gvGroupDeleteRow" DataKeyNames="GroupID" BackColor="White">
            <HeaderStyle  BackColor="Gray" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="GroupID" HeaderText="ID" Visible="False"/>
                <asp:HyperLinkField DataNavigateUrlFields="GroupID" DataNavigateUrlFormatString="Group_photo.aspx?iid={0}"
                    DataTextField="GroupName" HeaderText="Group Name">
                    <HeaderStyle Width="160px" BackColor="Gray" ForeColor="White" />
                </asp:HyperLinkField>
                <asp:BoundField DataField="GroupDescription" HeaderText="Group Description">
                    <HeaderStyle Width="300px" BackColor="Gray" ForeColor="White" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Insertion">
                    <ItemTemplate>
                        <asp:Label ID="InsDate" runat="server" Text='<%# RelDte(DataBinder.Eval(Container, "DataItem.GroupInsertionDate")) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                       <asp:LinkButton id="btnDelete" CommandName="Delete" Runat="server" Text="Delete" OnClientClick="javascript : return confirm('Do you really want to \ndelete the item?');"/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
