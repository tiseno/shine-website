Imports System.IO
Imports BizShiny

Partial Class News_new
    Inherits System.Web.UI.Page

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If ImageUpload.HasFile Then
            Try
                Dim strFileName As String
                strFileName = ImageUpload.PostedFile.FileName
                Dim c As String = System.IO.Path.GetFileName(strFileName)
                c = c.Substring(0, c.LastIndexOf("."))
                Dim d As String = System.IO.Path.GetExtension(strFileName)
                Dim f As String = System.DateTime.Now.ToString("ddmmyy")
                Dim g As String = Left((System.DateTime.Now.ToString("hhmmssff")), 8)

                Dim fn As String = f + g + c + d
                Dim SaveLocation As String = Server.MapPath("..\Img") & "\" & fn

                ImageUpload.PostedFile.SaveAs(SaveLocation)
                'Response.Write("The file has been uploaded.")
                'Response.Write("Img/" + fn)
                LoadDataList()

            Catch ex As Exception
                Me.lblMsg.Text = "File upload FAILED."
            End Try
        Else
            Me.lblMsg.Text = "Please specify a valid file."
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            LoadDataList()
        End If
    End Sub
    Private Sub LoadDataList()

        Dim imagesFolder As String = MapPath("../Img")

        Dim objDI As New DirectoryInfo(imagesFolder)

        Dim files() As FileInfo = objDI.GetFiles()
        Dim dsfiles(0 To files.Length - 1) As FileInfo

        Dim i As Integer = 0
        Dim j As Integer = 0

        For i = 0 To files.Length - 1
            If files(i).LastWriteTime.ToShortDateString = DateTime.Now.ToShortDateString Then
                'If files(i).LastWriteTime.ToShortDateString = "15/12/2009" Then
                dsfiles(j) = files(i)
                j = j + 1
            End If
            'For files(i).LastWriteTime.ToShortDateString = "15/12/2009" To "24/12/2009"
            '    dsfiles(j) = files(i)
            '    j = j + 1
            'Next

        Next
        Dim dsfiles1(0 To j - 1) As FileInfo
        For i = 0 To j - 1
            dsfiles1(i) = dsfiles(i)
        Next
        'dlImages.DataSource = objDI.GetFiles()
        dlImages.DataSource = dsfiles1
        dlImages.DataBind()

    End Sub

    Protected Sub btn_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Submit.Click
        Dim bn As New BizNews()
        Dim script As String = Nothing
        Dim message As String = Nothing
        Try
            bn.insertNews(txt_NewsTitle.Text, FreeTextBox1.Text)

            message = "Submited successfully."
            script = "<script type=""text/javascript"">alert('" & message & "');window.location='News_new.aspx';</script>"
            Me.ClientScript.RegisterStartupScript(Me.GetType(), "News", script)
        Catch ex As Exception

        End Try

    End Sub
End Class
