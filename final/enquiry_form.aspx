<%@ Page Language="VB" AutoEventWireup="false" CodeFile="enquiry_form.aspx.vb" Inherits="enquiry_form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #d1d1d1;
	margin-top: 0px;
}
-->
</style>
<meta NAME="KEYWORDS" CONTENT="Special needs, One-stop centre,Early intervention, Special education, Speech therapy,Occupational therapy, Music therapy, Clinical child psychology, Assessments, Autism, Aspergerís, Learning difficulties, Learning disabilities, Developmental delays, ADD/ADHD, Speech, language and communication, Speech therapist, Occupational therapist, Clinical psychologist, Special education teacher, Behaviour management and modification, Parent training, School readiness programme, Self-help skills, Play skills, Social skills, Speech delay, Diagnosis, Malaysia" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 545px">
            <tr>
                <td style="width: 450px; height: 311px" valign="top" class="content">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 535px">
                        <tr>
                            <td style="height: 40px; width: 100px;" valign="top">
                                <strong>Name:&nbsp; </strong><span class="red" style="color: red">*</span>
                            </td>
                            <td style="height: 40px; width: 525px;">
                                <asp:TextBox ID="texName" runat="server" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="NameRequiredFieldValidator" runat="server" ControlToValidate="texName"
                                    ErrorMessage="* Enter your name." Font-Size="Smaller"></asp:RequiredFieldValidator>
                                &nbsp;
                                &nbsp;<strong>&nbsp;
                                    <br />
                                </strong>
                                <asp:RegularExpressionValidator ID="valRegEx1" runat="server" BackColor="Transparent"
                                    BorderColor="Transparent" BorderStyle="None" ControlToValidate="texName" ErrorMessage="Your entry is not a valid name."
                                    Font-Size="Smaller" ValidationExpression="^[a-zA-Z''-'\s]{1,40}$"></asp:RegularExpressionValidator>
                                </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 100px">
                                <strong>Tel:</strong></td>
                            <td style="width: 525px">
                                <asp:TextBox ID="texTel" runat="server" Width="139px"></asp:TextBox>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                &nbsp;</td>
                            <td style="width: 525px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="top" style="height: 37px; width: 100px;">
                                <strong>Email:&nbsp; <span class="red" style="color: red">*</span></strong></td>
                            <td style="height: 37px; width: 525px;">
                                <asp:TextBox ID="texEmail" runat="server" Width="300px"></asp:TextBox>&nbsp;
                                <asp:RequiredFieldValidator ID="EmailRequiredFieldValidator" runat="server" ControlToValidate="texEmail"
                                    ErrorMessage="* Enter your email address." Font-Size="Smaller">
    </asp:RequiredFieldValidator><br />
                                <asp:RegularExpressionValidator ID="valRegEx" runat="server" ControlToValidate="texEmail"
                                    ErrorMessage="Your entry is not a valid e-mail address." Font-Size="Smaller"
                                    ValidationExpression=".*@.*\..*">
    </asp:RegularExpressionValidator>
                                </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="420">
                        <tr>
                            <td valign="top" style="width: 90px; height: 105px;">
                                <strong>Enquiry:</strong></td>
                            <td width="340" style="height: 105px">
                                <asp:TextBox ID="texComment" runat="server" Height="112px" TextMode="MultiLine" Width="243px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 90px; height: 55px;">
                                <span style="color: #ff0000">
                                    <br />
                                    <br />
                                    </span>
                                <br />
                            </td>
                            <td width="340" style="padding-right:40px; height: 55px;">
                                <br />
                                <asp:Button ID="butSubmit" runat="server" Text="Submit"/><br />
                                <span style="color: #ff0000">* mandatory</span><br />
                            </td>
                        </tr>
                    </table>
                    
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
