<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Gallery.aspx.vb" Inherits="Gallery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SHINE Child Guidance Centre | Gallery</title>
<!-- InstanceEndEditable -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: whitesmoke;
	margin-top: 0px;
}
-->
</style>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<!-- InstanceBeginEditable name="head" -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="styles/lightbox.css" type="text/css" media="screen" />
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<script type="text/javascript" src="scripts/prototype.js"></script>
<script type="text/javascript" src="scripts/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="scripts/lightbox.js"></script>
</head>
<body onload="MM_preloadImages('images/btn_01_ro.jpg','images/btn_02_ro.jpg','images/btn_03_ro.jpg','images/btn_04_ro.jpg','images/btn_05_ro.jpg','images/btn_06_ro.jpg','images/btn_07_ro.jpg','images/btn_08_ro.jpg')">
<form id="form1" runat="server">
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="LightGrey">
  <tr>
   <td background="images/header_01.jpg"></td>
  </tr>
  <tr>
     <td background="images/header_02.jpg"><table width="960" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td width="42" height="84">&nbsp;</td>
         <td width="186" height="84"><a href="index.aspx"><img src="images/logo.jpg" alt="Logo" width="186" height="84" border="0" /></a></td>
         <td background="images/slogan.jpg" width="372" height="84">&nbsp;</td>
         <td background="images/address.jpg" width="185" height="84">&nbsp;</td>
         <td background="images/phone.jpg" width="133" height="84">&nbsp;</td>
         <td width="39" height="84">&nbsp;</td>
       </tr>
     </table></td>
  </tr>
  <tr>
    <td background="images/divide.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td width="41" height="19" class="html/header_bgimage03"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="41"><img src="images/btn_00.jpg" width="41" height="19" /></td>
        <td width="54"><a href="index.aspx"><img src="images/btn_01.jpg" alt="Home" name="Home" width="54" height="19" border="0" id="Home" onmouseover="MM_swapImage('Home','','images/btn_01_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="76"><a href="html/about.html"><img src="images/btn_02.jpg" alt="About Us" name="About_us" width="76" height="19" border="0" id="About_us" onmouseover="MM_swapImage('About_us','','images/btn_02_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/services.html"><img src="images/btn_03.jpg" alt="Our Services" name="Our_services" width="110" height="19" border="0" id="Our_services" onmouseover="MM_swapImage('Our_services','','images/btn_03_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/team.html"><img src="images/btn_04.jpg" alt="Our Team" name="Our_team" width="75" height="19" border="0" id="Our_team" onmouseover="MM_swapImage('Our_team','','images/btn_04_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/link.html"><img src="images/btn_05.jpg" alt="Link &amp; Resources" name="Link" width="125" height="19" border="0" id="Link" onmouseover="MM_swapImage('Link','','images/btn_05_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/contact.html"><img src="images/btn_06.jpg" alt="Contact Us" name="Contact" width="90" height="19" border="0" id="Contact" onmouseover="MM_swapImage('Contact','','images/btn_06_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/faq_child.html"><img src="images/btn_07.jpg" alt="FAQ" name="FAQ" width="40" height="19" border="0" id="FAQ" onmouseover="MM_swapImage('FAQ','','images/btn_07_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/career.html"><img src="images/btn_08.jpg" alt="Career Opportunities" name="Career" width="170" height="19" border="0" id="Career" onmouseover="MM_swapImage('Career','','images/btn_08_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="58"><img src="images/btn08.jpg" width="39" height="19" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
   <td background="images/header_04.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="42" height="264">&nbsp;</td>
        <td valign="top"><!-- InstanceBeginEditable name="Edit03" -->
         <table width="879" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><img src="images/banner/gallery.jpg" width="879" height="264" alt="Banner" /></td>
            </tr>
          </table>
		<!-- InstanceEndEditable --></td>
        <td width="39" height="64">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="60" height="182">&nbsp;</td>
        <td valign="top" style="width: 168px"><table width="168" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><!-- InstanceBeginEditable name="Edit01" -->
            <table width="168" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><a href="html/about.html"><img src="images/abt_us/sub_btn_01.jpg" alt="About Us" name="about" width="168" height="19" border="0" id="about" onmouseover="MM_swapImage('about','','images/abt_us/sub_btn_01_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
                </tr>
                <tr>
                  <td background="images/sub_btn_devide.jpg" width="168" height="9"></td>
                </tr>
                <tr>
                  <td width="168" height="10"></td>
                </tr>
                <tr>
                  <td><a href="html/abt_mission.html"><img src="images/abt_us/sub_btn_02.jpg" alt="About Us" name="mission" width="168" height="19" border="0" id="mission" onmouseover="MM_swapImage('mission','','images/abt_us/sub_btn_02_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
                </tr>
                <tr>
                  <td background="images/sub_btn_devide.jpg" width="168" height="9"></td>
                </tr>
                <tr>
                  <td width="168" height="10"></td>
                </tr>
                <tr>
                   <td style="height: 19px"><a href="html/abt_vision.html"><img src="images/abt_us/sub_btn_03.jpg" alt="About Us" name="vision" width="168" height="19" border="0" id="vision" onmouseover="MM_swapImage('vision','','images/abt_us/sub_btn_03_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
                </tr>
                <tr>
                  <td background="images/sub_btn_devide.jpg" width="168" height="9"></td>
                </tr>
                <tr>
                  <td width="168" height="10"></td>
                </tr>
                <tr>
                   <td><a href="html/abt_facilities.html"><img src="images/abt_us/sub_btn_04.jpg" alt="About Us" name="facilities" width="168" height="19" border="0" id="facilities" onmouseover="MM_swapImage('facilities','','images/abt_us/sub_btn_04_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
                </tr>
                <tr>
                  <td background="images/sub_btn_devide.jpg" width="168" height="9"></td>
                </tr>
                <tr>
                  <td width="168" height="10"></td>
                </tr>
                <tr>
                   <td style="height: 19px"><img src="images/abt_us/sub_btn_05_ro.jpg" alt="About Us" name="gallery" width="168" height="19" border="0" id="gallery" onmouseover="MM_swapImage('gallery','','images/abt_us/sub_btn_05_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></td>
                </tr>
              </table>
			<!-- InstanceEndEditable --></td>
          </tr>
        </table></td>
        <td width="77" valign="top">&nbsp;</td>
        <td valign="top" style="width: 580px"><table width="580" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><!-- InstanceBeginEditable name="Edit02" -->
             <table width="580" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top" style="height: 36px"><br /><p class="content_title">OUR GALLERY</p>
                    </td>
                </tr>
              </table>
			<!-- InstanceEndEditable -->
			            <asp:DataList ID="dt_Group" runat="server">
                <ItemTemplate>
                <table width="451px">
                    <tr>
                        <td colspan="3" height="50px">
                            <asp:Label ID="lbl_GroupDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GroupName") %>' Font-Names="verdana" Font-Size="9pt" Font-Bold="false"></asp:Label>
                            <br /><asp:Label ID="lbl_GroupName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GroupDescription") %>' Font-Names="verdana" Font-Size="8pt"></asp:Label>
                             <br /><asp:Label ID="Label1" runat="server" Text='<%# RelDte(DataBinder.Eval(Container, "DataItem.GroupInsertionDate")) %>' Font-Names="verdana" Font-Size="8pt"></asp:Label>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4" style=" height:20px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style=" height:20px;">
                            <asp:DataList ID="dt_Img" runat="server" DataSource='<%# GetImg(DataBinder.Eval(Container, "DataItem.GroupID")) %>' RepeatColumns="7">
                                <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 70px" align="center">
                                            <a href='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>' rel="lightbox[mini]" title='<%# DataBinder.Eval(Container, "DataItem.ImageName") %>'><img src='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>' width="65px" height="65px" border="0" /></a>
                                        </td>
                                    </tr>
                                </table>
                                </ItemTemplate>
                            </asp:DataList> 
                        </td>
                    </tr>
                </table>
                <br />
                </ItemTemplate>
            </asp:DataList>   
			</td>
          </tr>
        </table> </td>
        <td width="75">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="42" height="57" class="header_bgimage05">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 928px">
            <tr>
                <td class="footer_copyright" style="width: 104px; height: 19px">
                    &nbsp;</td>
                <td class="footer_copyright" style="width: 2294px; height: 19px">
                    @ 2009 SHINE Child Guidance Centre 
                </td>
                <td style="width: 1603px; height: 19px">
                    &nbsp;</td>
                <td style="height: 19px" width="528">
                </td>
            </tr>
            <tr>
                <td class="footer_add" style="width: 104px; height: 12px">
                    &nbsp;</td>
                <td class="footer_add" style="width: 2294px; height: 12px">
                    Jaya One, J-29-2, Block J No. 72A, Jalan Universiti, Petaling Jaya, 46200, Selangor.</td>
                <td class="footer_add" style="width: 1603px; height: 12px">
                    Office : 03 - 7960 8809 / 7960 9809   FAX : 03 - 7960 6809 
                </td>
                <td class="footer_add" style="height: 12px">
                    Email: info@shine.my</td>
            </tr>
        </table>
    </td>
  </tr>
</table>
</form>
</body>
</html>
