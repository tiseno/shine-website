﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="index.aspx.vb" Inherits="index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/shine_home.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>SHINE Child Guidance Centre |</title>
<!-- InstanceEndEditable -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: whitesmoke;
	margin-top: 0px;
}
-->
</style>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<!-- InstanceBeginEditable name="head" -->
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
<meta NAME="KEYWORDS" CONTENT="Special needs, One-stop centre,Early intervention, Special education, Speech therapy,Occupational therapy, Music therapy, Clinical child psychology, Assessments, Autism, Asperger’s, Learning difficulties, Learning disabilities, Developmental delays, ADD/ADHD, Speech, language and communication, Speech therapist, Occupational therapist, Clinical psychologist, Special education teacher, Behaviour management and modification, Parent training, School readiness programme, Self-help skills, Play skills, Social skills, Speech delay, Diagnosis, Malaysia" />
<!-- InstanceEndEditable -->
</head>

<body onload="MM_preloadImages('images/btn_01_ro.jpg','images/btn_02_ro.jpg','images/btn_03_ro.jpg','images/btn_04_ro.jpg','images/btn_05_ro.jpg','images/btn_06_ro.jpg','images/btn_07_ro.jpg','images/btn_08_ro.jpg')">
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="LightGrey">
  <tr>
   <td background="images/header_01.jpg"></td>
  </tr>
  <tr>
     <td background="images/header_02.jpg"><table width="960" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td height="84" style="width: 42px">&nbsp;</td>
         <td width="186" height="84"><a href="index.aspx"><img src="images/logo.jpg" alt="Logo" width="186" height="84" border="0" /></a></td>
         <td background="images/slogan.jpg" width="372" height="84">&nbsp;</td>
         <td background="images/address.jpg" width="185" height="84">&nbsp;</td>
         <td background="images/phone.jpg" width="133" height="84">&nbsp;</td>
         <td width="39" height="84">&nbsp;</td>
       </tr>
     </table></td>
  </tr>
  <tr>
    <td background="images/divide.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td width="41" height="19" class="../html/header_bgimage03"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="41"><img src="images/btn_00.jpg" width="41" height="19" /></td>
        <td width="54"><a href="index.aspx"><img src="images/btn_01.jpg" alt="Home" name="Home" width="54" height="19" border="0" id="Home" onmouseover="MM_swapImage('Home','','images/btn_01_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="76"><a href="html/about.html"><img src="images/btn_02.jpg" alt="About Us" name="About_us" width="76" height="19" border="0" id="About_us" onmouseover="MM_swapImage('About_us','','images/btn_02_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/services.html"><img src="images/btn_03.jpg" alt="Our Services" name="Our_services" width="110" height="19" border="0" id="Our_services" onmouseover="MM_swapImage('Our_services','','images/btn_03_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/team.html"><img src="images/btn_04.jpg" alt="Our Team" name="Our_team" width="75" height="19" border="0" id="Our_team" onmouseover="MM_swapImage('Our_team','','images/btn_04_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/link.html"><img src="images/btn_05.jpg" alt="Link &amp; Resources" name="Link" width="125" height="19" border="0" id="Link" onmouseover="MM_swapImage('Link','','images/btn_05_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/contact.html"><img src="images/btn_06.jpg" alt="Contact Us" name="Contact" width="90" height="19" border="0" id="Contact" onmouseover="MM_swapImage('Contact','','images/btn_06_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/faq_child.html"><img src="images/btn_07.jpg" alt="FAQ" name="FAQ" width="40" height="19" border="0" id="FAQ" onmouseover="MM_swapImage('FAQ','','images/btn_07_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="20"><img src="images/btn_divide.jpg" width="20" height="19" /></td>
        <td width="44"><a href="html/career.html"><img src="images/btn_08.jpg" alt="Career Opportunities" name="Career" width="170" height="19" border="0" id="Career" onmouseover="MM_swapImage('Career','','images/btn_08_ro.jpg',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        <td width="58"><img src="images/btn08.jpg" width="39" height="19" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
   <td background="images/header_04.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="42" height="264">&nbsp;</td>
        <td valign="top"><!-- InstanceBeginEditable name="Edit03" -->
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="877" height="360" id="FlashID" title="Banner">
            <param name="movie" value="flash/flash.swf" />
            <param name="quality" value="high" />
            <param name="wmode" value="opaque" />
            <param name="swfversion" value="9.0.45.0" />
            <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
            <param name="expressinstall" value="Scripts/expressInstall.swf" />
            <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
            <!--[if !IE]>-->
            <object type="application/x-shockwave-flash" data="flash/flash.swf" width="877" height="360">
              <!--<![endif]-->
              <param name="quality" value="high" />
              <param name="wmode" value="opaque" />
              <param name="swfversion" value="9.0.45.0" />
              <param name="expressinstall" value="Scripts/expressInstall.swf" />
              <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
              <div>
                <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
              </div>
              <!--[if !IE]>-->
            </object>
            <!--<![endif]-->
          </object>
          <script type="text/javascript">
<!--
swfobject.registerObject("FlashID");
//-->
          </script>
        <!-- InstanceEndEditable --></td>
        <td width="39" height="64">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center"><table width="90%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
        <td width="50%" align="left">
            <strong><span style="color: #3f403f; font-family: Arial">Our Mission</span></strong></td>
        <td>&nbsp;&nbsp;&nbsp;</td>
        <td width="50%" align="left">
            <strong><span style="color: #3f403f; font-family: Arial">Latest News</span></strong></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td valign="top" align="left">
        <font style="font-family:Verdana;color: #3f403f;"><span style="font-size: 8pt">SHINE Child Guidance Centre is a one-stop multidisciplinary centre for children
            and adolescents with special needs. The centre is wholly owned by Nilai Healthcare Services Sdn.
            Bhd. and was initiated by Tan Sri Dato Dr Gan Kong Seng, the Executive
            Chairman of Nilai Resources Group Berhad. <br /><a href="html/about.html">Read More</a></span></font>
            </td>
            <td>&nbsp;</td>
       <td height="90" valign="top" style="background-color: Transparent; border-right: darkgray 1px solid; border-top: darkgray 1px solid; border-left: darkgray 1px solid; border-bottom: darkgray 1px solid;">
        <MARQUEE behavior="scroll" height="90px" direction="up" scrollamount="3" onmouseover="this.stop();" onmouseout="this.start();">
            <asp:DataList ID="dtNews" runat="server">
                <ItemTemplate>
                    <table width="500px">
                    <tr>
                        <td width="10px">-</td>
                        <td width="30px">
                            <asp:Label ID="Label1" runat="server" Text='<%# RelDte(DataBinder.Eval(Container, "DataItem.NewsInsertionDate")) %>' Font-Names="verdana" Font-Size="8pt"></asp:Label>
                        </td>                        
                        <td width="235px">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# ProUrl(DataBinder.Eval(Container, "DataItem.NewsID")) %>'><asp:Label ID="lblTitle" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NewsTitle") %>' Font-Names="verdana" Font-Size="8pt"></asp:Label></asp:HyperLink>
                        </td>                    
                    </tr>
                    </table>
                    <hr /> 
                </ItemTemplate>
            </asp:DataList>
        </MARQUEE>       
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="42" height="57" class="header_bgimage05"><table border="0" cellspacing="0" cellpadding="0" style="width: 928px">
      <tr>
        <td class="footer_copyright" style="height: 19px; width: 104px;">&nbsp;</td>
        <td class="footer_copyright" style="width: 2294px; height: 19px">@ 2009 SHINE Child Guidance Centre </td>
        <td style="width: 1603px; height: 19px">&nbsp;</td>
          <td style="height: 19px" width="528">
          </td>
      </tr>
      <tr>
        <td class="footer_add" style="height: 12px; width: 104px;">&nbsp;</td>
        <td class="footer_add" style="width: 2294px; height: 12px">Jaya One, J-29-2, Block J No. 72A, Jalan Universiti, Petaling Jaya, 46200, Selangor.</td>
        <td class="footer_add" style="width: 1603px; height: 12px">Office : 03 - 7960 8809 / 7960 9809   FAX : 03 - 7960 6809 </td>
          <td class="footer_add" style="height: 12px">
              Email: info@shine.my</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>

