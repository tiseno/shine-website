<%@ Page Language="VB" AutoEventWireup="false" CodeFile="News_detail.aspx.vb" Inherits="News_detail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SHINE Child Guidance Centre | News</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #d1d1d1;
	margin-top: 30px;
	margin-left: 30px;
}
-->
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="0" border="0" cellspacing="0">
            <tr>
                <td style="width: 238px; height: 21px;" class="subline02">
                <asp:Label ID="lbl_title" runat="server" Text=""></asp:Label></td>
                <td style="width: 82px; height: 21px;">
                </td>
                <td style="width: 228px; height: 21px;" align="right" class="subline02">
                <asp:Label ID="lbl_Idate" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 238px; height: 15px">
                </td>
                <td style="width: 82px; height: 15px">
                </td>
                <td align="right" style="width: 228px; height: 15px">
                </td>
            </tr>
            <tr>
                <td style="height: 20px" colspan="3"><hr />
                </td>
            </tr>
            <tr>
                <td colspan="3" rowspan="2" class="content">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Width="530px">
                        <asp:Literal ID="news_body" runat="server"></asp:Literal>
                </asp:Panel>
                </td>
            </tr>
            <tr>
            </tr>
        </table>
        </div>
    </form>
</body>
</html>
