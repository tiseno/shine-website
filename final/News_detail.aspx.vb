Imports CommonShiny
Imports BizShiny
Imports DataShiny
Imports System.Data

Partial Class News_detail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim id As String = Request("news_id")
        If Not IsPostBack Then
            BindData(id)
        End If
    End Sub

    Private Sub BindData(ByVal IID As String)
        Dim bn As New BizNews()
        Dim cn As CommonNews = bn.selectNewsByID(IID)

        If Not cn Is Nothing Then
            lbl_title.Text = cn.Title
            lbl_Idate.Text = cn.InsertionDate
            news_body.Text = cn.Body
        End If
    End Sub

End Class
