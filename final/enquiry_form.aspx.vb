Imports System.Net.Mail
Imports System.Text.RegularExpressions

Partial Class enquiry_form
    Inherits System.Web.UI.Page

    Protected Sub butSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Const ToAddress As String = "zack@quality-direct.net" '///EMAIL ADD///'
            Dim mm As New MailMessage()
            mm.Subject = "Enquiry from Shine Web"
            mm.To.Add(ToAddress)
            mm.From = New MailAddress(texEmail.Text)
            Dim body As String = "Name: " & texName.Text & vbCrLf
            body += "Tel: " & texTel.Text & vbCrLf
            body += "Enquiry: " & texComment.Text

            mm.Body = body
            mm.IsBodyHtml = False

            Dim smtp As New SmtpClient("mail.shine.my", 25)
            smtp.Send(mm)
            Response.Write("<SCR" + "IPT language='javascript'>alert('Thank you for the enquiry!') ;window.location='enquiry_form.aspx';</SCR" + "IPT>")

        Catch ex As Exception
            Response.Write("<SCR" + "IPT language='javascript'>alert('Email Failure! Please check your E-mail! ');</SCR" + "IPT>")
        End Try
    End Sub
End Class
