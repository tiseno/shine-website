Imports CommonShiny
Imports BizShiny
Imports DataShiny
Imports System.Data

Partial Class index
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        BindData()
    End Sub

    Private Sub BindData()
        Dim bn As New BizNews()
        Dim stDate As String = DateTime.Now.AddMonths(-6)
        Dim enDate As String = DateTime.Now()
        Dim dt As DataSet

        Try

            dt = bn.findNews("", stDate, enDate)
            ''dtNews.DataSource = dt
            ''dtNews.DataBind()

        Catch ex As Exception

        End Try

    End Sub

    Function RelDte(ByVal dte)

        Dim InsDate As String
        InsDate = Right(dte, 2) + "/" + Mid(dte, 5, 2) + "/" + Left(dte, 4)

        Return InsDate

    End Function

    Function ProURL(ByVal URL)
        Return "javascript:void( window.open( 'News_detail.aspx?news_id=" & URL & "', '', 'width=600,top=1,left=1,scrollbars=yes, toolbar=no, menubar=no, resizable=yes' ))"
    End Function

End Class
