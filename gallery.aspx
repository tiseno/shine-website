<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Gallery.aspx.vb" Inherits="Gallery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SHINE Child Guidance Centre | Gallery</title>
<!-- InstanceEndEditable -->
<link href="css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<!-- InstanceBeginEditable name="head" -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="styles/lightbox.css" type="text/css" media="screen" />
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<script type="text/javascript" src="scripts/prototype.js"></script>
<script type="text/javascript" src="scripts/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="scripts/lightbox.js"></script>
</head>
<body onload="MM_preloadImages('images/btn_01_ro.jpg','images/btn_02_ro.jpg','images/btn_03_ro.jpg','images/btn_04_ro.jpg','images/btn_05_ro.jpg','images/btn_06_ro.jpg','images/btn_07_ro.jpg','images/btn_08_ro.jpg')">
<div class="wrapper">
<form id="form1" runat="server">
<table class="page_frame" border="0" cellpadding="0" cellspacing="0" align="center" >
  <tr>
   <td background="images/header_01.jpg"></td>
  </tr>
  <tr>
     <td background="images/header_02.jpg"><table width="960" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td width="42" height="84">&nbsp;</td>
         <td width="186" height="84"><a href="index.aspx"><img src="images/logo.jpg" alt="Logo" width="186" height="84" border="0" /></a></td>
         <td background="images/slogan.jpg" width="372" height="84">&nbsp;</td>
         <td background="images/address.jpg" width="185" height="84">&nbsp;</td>
         <td background="images/phone.jpg" width="133" height="84">&nbsp;</td>
         <td width="39" height="84">&nbsp;</td>
       </tr>
     </table></td>
  </tr>
  <tr>
    <td background="images/divide.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td width="41" height="19" class="html/header_bgimage03"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="nav_bar">
            <div class="mainmenu" align="center">
            <ul>
                <li><a href="index.aspx">HOME</a></li>
                <li><a href="html/about.html">ABOUT US</a></li>
                <li><a href="html/services.html">OUR SERVICES</a></li>
                <li><a href="html/team.html">OUR TEAM</a></li>
                <li><a href="html/contact.html">CONTACT</a></li>
                <li><a href="html/faq_enrolment.html">FAQ</a></li>
                <li><a href="html/career.html">CAREER</a></li>
                <li><a href="#" id="maincurrent">GALLERY</a></li>
                <li style="padding-right:0px; margin-right:0px;"><a href="html/upcoming_event.html">EVENTS / PROGRAMS</a></li>
            </ul>
    	</div>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
   <td background="images/header_04.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <!--Banner Here-->
        <!--<td width="42" height="264">&nbsp;</td>
        <td valign="top"><!-- InstanceBeginEditable name="Edit03" -->
        <!--<table width="879" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td><img src="images/banner/gallery.jpg" width="879" height="264" alt="Banner" /></td>
            </tr>
          </table>
		<!-- InstanceEndEditable --><!--</td>
        <td width="39" height="64">&nbsp;</td>-->
        <!--Banner End Here-->
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="60" height="182">&nbsp;</td>
        <td valign="top" style="width: 168px"><table width="168" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><!-- InstanceBeginEditable name="Edit01" -->
            <table border="0" cellspacing="0" cellpadding="0">
            <tr><td style="padding:20px 0px 0px 0px;">
            <p class="content_title">OUR FACILITIES</p>
                  
                    <!--<p class="content">SHINE has the following  facilities:</p>-->
                    
                    <ul style="padding-left:0; margin-left:15px;">
                      <li class="content">Consultation rooms </li>
                      <li class="content">Individual teaching rooms</li>
                      <li class="content">Group therapy rooms</li>
                      <li class="content">Computer lab</li>
                      <li class="content">Playroom</li>
                      <li class="content">Resource Room for parents</li>
                      <li class="content">Multipurpose lecture hall</li>
                    </ul><br />
            </td>
            </tr>
            </table>
			<!-- InstanceEndEditable --></td>
          </tr>
        </table></td>
        <td width="77" valign="top">&nbsp;</td>
        <td valign="top" style="width: 580px"><table width="580" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><!-- InstanceBeginEditable name="Edit02" -->
             <table width="580" border="0" cellspacing="0" cellpadding="0">
             <tr>
             <td valign="top"></td>
             </tr>
                <tr>
                  <td valign="top" style="height: 36px"><br /><p class="content_title">OUR GALLERY</p>
                    </td>
                </tr>
              </table>
			<!-- InstanceEndEditable -->
			            <asp:DataList ID="dt_Group" runat="server">
                <ItemTemplate>
                <table width="451px">
                    <tr>
                        <td colspan="3" height="50px">
                            <asp:Label ID="lbl_GroupDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GroupName") %>' Font-Names="verdana" Font-Size="9pt" Font-Bold="false"></asp:Label>
                            <br /><asp:Label ID="lbl_GroupName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GroupDescription") %>' Font-Names="verdana" Font-Size="8pt"></asp:Label>
                             <br /><asp:Label ID="Label1" runat="server" Text='<%# RelDte(DataBinder.Eval(Container, "DataItem.GroupInsertionDate")) %>' Font-Names="verdana" Font-Size="8pt"></asp:Label>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4" style=" height:20px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style=" height:20px;">
                            <asp:DataList ID="dt_Img" runat="server" DataSource='<%# GetImg(DataBinder.Eval(Container, "DataItem.GroupID")) %>' RepeatColumns="7">
                                <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 70px" align="center">
                                            <a href='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>' rel="lightbox[mini]" title='<%# DataBinder.Eval(Container, "DataItem.ImageName") %>'><img src='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>' width="65px" height="65px" border="0" /></a>
                                        </td>
                                    </tr>
                                </table>
                                </ItemTemplate>
                            </asp:DataList> 
                        </td>
                    </tr>
                </table>
                <br />
                </ItemTemplate>
            </asp:DataList>  <br /><br /> 
			</td>
          </tr>
        </table> </td>
        <td width="75">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr class="footer_position">
    <td width="42" height="57" class="header_bgimage05">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 960px">
            <tr>
                <td class="footer_copyright" style="width: 104px; height: 19px">
                    &nbsp;</td>
                <td class="footer_copyright" style="width: 2294px; height: 19px">
                    @ 2012 SHINE Guidance Centre 
                </td>
                <td style="width: 1603px; height: 19px">
                    &nbsp;</td>
                <td style="height: 19px" width="528">
                </td>
            </tr>
            <tr>
                <td class="footer_add" style="width: 104px; height: 12px">
                    &nbsp;</td>
                <td class="footer_add" style="width: 2294px; height: 12px">
                    Jaya One, J-29-2, Block J No. 72A, Jalan Universiti, Petaling Jaya, 46200, Selangor.</td>
                <td class="footer_add" style="width: 1603px; height: 12px">
                    Office : 03 - 7960 8809 / 7960 9809   FAX : 03 - 7960 6809 
                </td>
                <td class="footer_add" style="height: 12px">
                    Email: info@shine.my</td>
            </tr>
        </table>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>
