﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="internships.aspx.vb" Inherits="html_internships" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>SHINE Child Guidance Centre | Career Opportunities</title>
<!-- InstanceEndEditable -->
<link href="../css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<!-- InstanceBeginEditable name="head" -->
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<meta NAME="KEYWORDS" CONTENT="Special needs, One-stop centre,Early intervention, Special education, Speech therapy,Occupational therapy, Music therapy, Clinical child psychology, Assessments, Autism, Asperger’s, Learning difficulties, Learning disabilities, Developmental delays, ADD/ADHD, Speech, language and communication, Speech therapist, Occupational therapist, Clinical psychologist, Special education teacher, Behaviour management and modification, Parent training, School readiness programme, Self-help skills, Play skills, Social skills, Speech delay, Diagnosis, Malaysia" />
<!-- InstanceEndEditable -->
</head>
<body onload="MM_preloadImages('../images/btn_01_ro.jpg','../images/btn_02_ro.jpg','../images/btn_03_ro.jpg','../images/btn_04_ro.jpg','../images/btn_05_ro.jpg','../images/btn_06_ro.jpg','../images/btn_07_ro.jpg','../images/btn_08_ro.jpg','../images/career/sub_btn_01_ro.jpg','../images/career/sub_btn_02_ro.jpg','../images/career/sub_btn_03_ro.jpg','../images/career/sub_btn_04_ro.jpg')">
    <div class="wrapper">
    <form id="form1" runat="server">
    <table class="page_frame" border="0" cellpadding="0" cellspacing="0" align="center" >
  <tr>
   <td background="../images/header_01.jpg"></td>
  </tr>
  <tr>
     <td background="../images/header_02.jpg"><table width="960" border="0" cellspacing="0" cellpadding="0">
       <tr>
     <td background="../images/header_02.jpg"><table width="960" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td width="41" height="84">&nbsp;</td>
         <td width="195" height="84"><a href="../index.aspx"><img src="../images/logo.jpg" alt="Logo" width="186" height="84" border="0" /></a></td>
         <td background="../images/slogan.jpg" width="372" height="84">&nbsp;</td>
         <td background="../images/address.jpg" width="185" height="84">&nbsp;</td>
         <td background="../images/phone.jpg" width="130" height="84">&nbsp;</td>
         <td width="37" height="84">&nbsp;</td>
       </tr>
     </table></td>
  </tr>
     </table></td>
  </tr>
  <tr>
    <td background="../images/divide.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td width="41" height="19" class="../html/header_bgimage03"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="nav_bar">
            <div class="mainmenu" align="center">
            <ul>
                <li><a href="../index.aspx">HOME</a></li>
                <li><a href="about.html">ABOUT US</a></li>
                <li><a href="services.html">OUR SERVICES</a></li>
                <li><a href="team.html">OUR TEAM</a></li>
                <li><a href="contact.html">CONTACT</a></li>
                <li><a href="faq_child.html">FAQ</a></li>
                <li><a href="#"  id="maincurrent">CAREER</a></li>
                <li><a href="../gallery.aspx">GALLERY</a></li>
                <li style="padding-right:0px; margin-right:0px;"><a href="upcoming_event.html" >EVENTS / PROGRAMS</a></li>
            </ul>
    	</div>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
   <td background="../images/header_04.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="42" height="264">&nbsp;</td>
        <td valign="top"><!-- InstanceBeginEditable name="Edit03" -->
         <table width="879" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><img src="../images/banner/career.jpg" width="879" height="264" alt="Banner" /></td>
            </tr>
          </table>
		  <!-- InstanceEndEditable --></td>
        <td width="39" height="64">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="60" height="182">&nbsp;</td>
        <td width="168" valign="top"><table width="168" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><!-- InstanceBeginEditable name="Edit01" -->
            <table class="side_bar" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><a href="career.html">CAREER</a><br /><img src="../images/sub_btn_devide.jpg" /></td>
                </tr>
                <tr>
                   <td><a href="#" id="side_active">INTERNSHIPS</a></td>
                </tr>
              </table>
			<!-- InstanceEndEditable --></td>
          </tr>
        </table></td>
        <td width="77" valign="top">&nbsp;</td>
        <td width="580" valign="top"><table width="580" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><!-- InstanceBeginEditable name="Edit02" -->
            <table width="580" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top"><br /><p class="content_title">INTERNSHIPS</p>
                    <p class="content">We are  looking for students currently pursuing degrees in Psychology, Special Education, or Early Childhood Education who would like to gain experience working with special needs children and adolescents at our centre. If you are  interested in applying for an internship at SHINE, please fill in the form  below:</p>                    
                    
                    <p class="content">To be eligible for an internship at SHINE, the applicant must</p>
                    <ul>
                      <li class="content">Be currently enrolled in an institution approved by the Public Service Department (JPA).</li>
                      <li class="content">Pursuing a Diploma, Bachelor’s degree, or Master’s degree in Psychology, Special Education, or Early Childhood Education.</li>
                      <li class="content">Be able to commit to an internship for the duration of at least one (1) term and minimum of 3 full days per week (schedule is negotiable).</li>
                      <li class="content">Sign an Agreement Form to abide by SHINE’s terms and conditions.</li>
                      <li class="content">Submit CV, a letter of referral (by a supervisor or lecturer), a letter from head of department (stating that the internship is a requirement of the program), and a letter from Registrar’s Office (confirming that the applicant is currently student in the institution).</li>
                    </ul>
                        <br />
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 545px">
                        <tr>
                        <td style="width: 450px; height: 311px" valign="top" class="content">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 535px">
                                <tr>
                                <td style="height: 40px; width: 312px;" valign="top">
                                <strong>Name:&nbsp; </strong><span class="red" style="color: red">*</span>
                                </td>
                                <td style="height: 40px; width: 525px;">
                                <asp:TextBox ID="texName" runat="server" Width="300px"></asp:TextBox>
                                    &nbsp; &nbsp;
                                &nbsp;<strong>&nbsp;
                                <br />
                                </strong>
                                <asp:RegularExpressionValidator ID="valRegEx1" runat="server" BackColor="Transparent"
                                BorderColor="Transparent" BorderStyle="None" ControlToValidate="texName" ErrorMessage="Your entry is not a valid name."
                                Font-Size="Smaller" ValidationExpression="^[a-zA-Z''-'\s]{1,40}$" Width="185px"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="NameRequiredFieldValidator" runat="server" ControlToValidate="texName"
                                ErrorMessage="* Enter your name." Font-Size="Smaller"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                <td valign="top" style="width: 312px">
                                <strong>Contact  number:</strong></td>
                                <td style="width: 525px">
                                <asp:TextBox ID="texTel" runat="server" Width="135px"></asp:TextBox>
                                <br />
                                </td>
                                </tr>
                                <tr>
                                <td style="width: 312px">
                                &nbsp;</td>
                                <td style="width: 525px">
                                &nbsp;</td>
                                </tr>
                                <tr>
                                <td valign="top" style="height: 37px; width: 312px;">
                                <strong>Email:&nbsp; <span class="red" style="color: red">*</span></strong></td>
                                <td style="height: 37px; width: 525px;">
                                <asp:TextBox ID="texEmail" runat="server" Width="207px"></asp:TextBox>&nbsp;
                                <br />
                                <asp:RegularExpressionValidator ID="valRegEx" runat="server" ControlToValidate="texEmail"
                                ErrorMessage="Your entry is not a valid e-mail address." Font-Size="Smaller"
                                ValidationExpression=".*@.*\..*">
                                </asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="EmailRequiredFieldValidator" runat="server" ControlToValidate="texEmail"
                                ErrorMessage="* Enter your email address." Font-Size="Smaller">
                                </asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                <td valign="top" style="width: 312px">
                                <strong>College/University:</strong></td>
                                <td style="width: 525px">
                                <asp:TextBox ID="txtCollege" runat="server" Width="300px"></asp:TextBox>
                                <br />
                                </td>
                                </tr>
                                <tr>
                                <td style="width: 312px">
                                &nbsp;</td>
                                <td style="width: 525px">
                                &nbsp;</td>
                                </tr>
                                <tr>
                                <td valign="top" style="width: 312px">
                                <strong>Degree Major:</strong></td>
                                <td style="width: 525px">
                                <asp:TextBox ID="txtDegree" runat="server" Width="300px"></asp:TextBox>
                                <br />
                                </td>
                                </tr>
                                <tr>
                                <td style="width: 312px">
                                &nbsp;</td>
                                <td style="width: 525px">
                                &nbsp;</td>
                                </tr>
                                <tr>
                                <td valign="top" style="width: 312px">
                                <strong>Main area of  interest:</strong></td>
                                <td style="width: 525px">
                                <asp:TextBox ID="txtAreaOfInterest" runat="server" Width="300px"></asp:TextBox>
                                <br />
                                </td>
                                </tr>
                                <tr>
                                <td style="width: 312px">
                                &nbsp;</td>
                                <td style="width: 525px">
                                &nbsp;</td>
                                </tr>
                                <tr>
                                <td valign="top" style="width: 312px; height: 29px;">
                                <strong>Availability:</strong></td>
                                <td style="width: 525px; height: 29px;" valign="top">
                                <asp:TextBox ID="txtAvailability" runat="server" Width="300px"></asp:TextBox>
                                <br />
                                </td>
                                </tr>
                                <tr>
                                <td style="width: 312px">
                                &nbsp;</td>
                                <td style="width: 525px">
                                <asp:Button ID="butSubmit" runat="server" Text="Submit" Width="79px"/><br />
                                    <span style="color: #ff0000">* mandatory</span></td>
                                </tr>
                                </table>

                        &nbsp;</td>
                        </tr>
                        </table></td>
                </tr>
              </table>
			<!-- InstanceEndEditable --></td>
          </tr>
        </table> </td>
        <td width="75">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr class="footer_position">
    <td width="42" height="57" class="header_bgimage05"><table border="0" cellspacing="0" cellpadding="0" style="width: 960px;">
      <tr>
        <td class="footer_copyright" style="height: 19px; width: 104px;">&nbsp;</td>
        <td class="footer_copyright" style="width: 2294px; height: 19px">@ 2009 SHINE Guidance Centre </td>
        <td style="width: 1603px; height: 19px">&nbsp;</td>
          <td style="height: 19px; width: 442px;">
          </td>
          <td style="height: 19px; width: 141px;">
          </td>
      </tr>
      <tr>
        <td class="footer_add" style="height: 12px; width: 104px;">&nbsp;</td>
        <td class="footer_add" style="width: 2294px; height: 12px">Jaya One, J-29-2, Block J No. 72A, Jalan Universiti, Petaling Jaya, 46200, Selangor.</td>
        <td class="footer_add" style="width: 1603px; height: 12px">Office : 03 - 7960 8809 / 7960 9809   
            
            FAX : 03 - 7960 6809 </td>
          <td class="footer_add" style="height: 12px; width: 442px;">
              Email: info@shine.my</td>
          <td class="footer_add" style="height: 12px; width: 141px;">
          <a href="http://www.quality-direct.net" style="text-decoration:none;"><img src="images/web_design.png" alt="web design by Quality Direct" border="0"/></a>
          </td>
      </tr>
    </table></td>
  </tr>
</table>
    </form>
    </div>
</body>
</html>
